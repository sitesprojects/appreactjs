## How to build this project

- cd docker
- bash build.sh $app_name $tag
- docker images
- docker run --rm -p 80:80 -d --name webApp $app_name:$tag
