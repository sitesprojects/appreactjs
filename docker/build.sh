APP_NAME=$1
APP_TAG=$2

rm -rf build
git pull origin master

echo "## Generando imagen $APP_NAME:$APP_TAG#"

cd ..
npm install
npm run build

cd Docker

cp -r ../build .
docker build . -t $APP_NAME:$APP_TAG
