import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class HelloApp extends React.Component {
  render() {
    return (
      <h1>Hello App - UY!</h1>
    );
  }
}



ReactDOM.render(
  <HelloApp />,
  document.getElementById('root')
);
